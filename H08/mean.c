#include <stdio.h>
#include <stdlib.h>

typedef struct vector_s
{
    int dim;
    float *data_ptr;
} vector_type;

/* Allocate and initialize to 0 a vector with the given dimension */
void vector_init_zeros(vector_type *v_ptr, int dim)
{
    v_ptr->dim = dim;
    v_ptr->data_ptr = (float *)calloc(dim, sizeof(float));
#ifdef DEBUG
    if (v_ptr->data_ptr == 0)
    {
        printf("vector_init_zeros error: calloc returned a null ptr\n");
        exit(1);
    }
#endif
}

/* Free up a vector that was allocated using vector_init_zeros */
void vector_deinit(vector_type *v_ptr)
{
    v_ptr->dim = 0;
    free(v_ptr->data_ptr);
    v_ptr->data_ptr = 0;
}

/* Attempt to read a vector from stdin. */
/* Returns 1 if read successful and 0 otherwise */
int vector_read(vector_type *v_ptr)
{
    int i;
    float next;
    for (i = 0; i < v_ptr->dim; i++)
    {
        if (scanf("%g\n", &next) != 1)
        {
            return 0;
        }
        v_ptr->data_ptr[i] = next;
    }
    return 1;
}

/* w = u + v */
void vector_add(vector_type *u_ptr, vector_type *v_ptr,
                vector_type *w_ptr)
{
    for (int i = 0; i < w_ptr->dim; i++)
    {
        w_ptr->data_ptr[i] = u_ptr->data_ptr[i] + v_ptr->data_ptr[i];
    }
}

/* v = cv */
void vector_scalar_mult(vector_type *v_ptr, float c)
{
    for (int i = 0; i < v_ptr->dim; i++)
    {
        v_ptr->data_ptr[i] = c * v_ptr->data_ptr[i];
    }
}

/* print the compondents of the given vector */
void vector_print(vector_type *v_ptr)
{
    // printf("The vector compondents: ");
    for (int i = 0; i < v_ptr->dim; i++)
    {
        printf("%g ", v_ptr->data_ptr[i]);
    }
    printf("\n");
}

int main()
{
    vector_type vector;
    vector_type next_vector;
    int dim;
    if (scanf("%d", &dim) != 1)
    {
        printf("Error reading the vector dimension from stdin\n");
        exit(1);
    }

    vector_init_zeros(&vector, dim);
    vector_init_zeros(&next_vector, dim);
    vector_read(&vector);
    int count = 1;
    while (vector_read(&next_vector) == 1)
    {
        vector_add(&vector, &next_vector, &vector);
        count++;

        vector_deinit(&next_vector);
        vector_init_zeros(&next_vector, dim);
    }

    float n = 1.0 / count;
    vector_scalar_mult(&vector, n);

    printf("The vector mean is: ");
    vector_print(&vector);
}
