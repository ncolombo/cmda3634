#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "matrix.h"
#include "maxheap.h"

/* find the k nearest neighbors sorted from closest to farthest */
void find_knearest(matrix_type *train_images_ptr, matrix_row_type *test_image_ptr,
                   int k, int *knearest)
{

    matrix_row_type train_image;
    key_value_type pair;
    maxheap_type heap;
    maxheap_init(&heap, k);
    int i;
    for (i = 0; i < train_images_ptr->num_rows; i++)
    {
        matrix_get_row(train_images_ptr, &train_image, i);
        pair.key = i;
        pair.value = matrix_row_dist_sq(test_image_ptr, &train_image);
        if (heap.size < k)
        {
            maxheap_insert(&heap, pair);
        }
        else if (pair.value < heap.array[0].value)
        {
            maxheap_remove_root(&heap);
            maxheap_insert(&heap, pair);
        }
    }

    /* store the k nearest neighbors from closest to farthest */
    for (i = k - 1; i >= 0; i--)
    {
        pair = heap.array[0];
        knearest[i] = pair.key;
        maxheap_remove_root(&heap);
    }

    /* free up the heap */
    maxheap_deinit(&heap);
}

/* classify a test image given the k nearest neighbors */
/* predict the class using the "majority rule" */
/* if there is a tie reduce k by 1 and repeat until a single class has a majority */
/* note that the tie breaking process is guaranteed to terminate when k=1 */
int classify(matrix_type *train_labels_ptr, int num_classes, int k, int *knearest)
{
    int class = 0;
    int class_labels[k];
    matrix_row_type current;
    for (int h = 0; h < k; h++)
    {
        matrix_get_row(train_labels_ptr, &current, knearest[h]);
        class_labels[h] = current.data_ptr[0];
    }

    int check = 0;
    while (check == 0)
    {
        class = 0;
        int highest = 0;
        for (int i = 0; i < num_classes; i++)
        {
            int count = 0;
            for (int j = 0; j < k; j++)
            {
                if (i == class_labels[j])
                {
                    count++;
                }
            }
            if (count > highest)
            {
                highest = count;
                class = i;
                check = 1;
            }
        }
        for (int i = 0; i < num_classes; i++)
        {
            int count2 = 0;
            for (int j = 0; j < k; j++)
            {
                if (i == class_labels[j])
                {
                    count2++;
                }
            }
            if (count2 == highest && class != i)
            {
                check = 0;
                k--;
            }
        }
    }

    return class;
}

int main(int argc, char **argv)
{

    /* get k, start test image, and num_to_test from the command line */
    if (argc != 4)
    {
        printf("Command usage : %s %s %s %s\n", argv[0], "k", "start_test", "num_to_test");
        return 1;
    }
    int k = atoi(argv[1]);
    int start_test = atoi(argv[2]);
    int num_to_test = atoi(argv[3]);
    if (num_to_test + start_test > 10000)
        num_to_test = 10000 - start_test;

    /* the MNIST dataset has 10 class labels */
    int num_classes = 10;

    /* read in the mnist training set of 60000 images and labels */
    int num_train = 60000;
    matrix_type train_images, train_labels;
    matrix_init(&train_images, num_train, 784);
    matrix_read_bin(&train_images, "train-images-idx3-ubyte", 16);
    matrix_init(&train_labels, num_train, 1);
    matrix_read_bin(&train_labels, "train-labels-idx1-ubyte", 8);

    /* read in the mnist test set of 10000 images */
    int num_test = 10000;
    matrix_type test_images, test_labels;
    matrix_init(&test_images, num_test, 784);
    matrix_read_bin(&test_images, "t10k-images-idx3-ubyte", 16);
    matrix_init(&test_labels, num_test, 1);
    matrix_read_bin(&test_labels, "t10k-labels-idx1-ubyte", 8);

    /* find the k training images nearest the given test image */
    int i, j;
    matrix_row_type test_image;
    int knearest[k];
    int predicted_label;
    for (i = start_test; i < start_test + num_to_test; i++)
    {
        matrix_get_row(&test_images, &test_image, i);
        find_knearest(&train_images, &test_image, k, knearest);
        predicted_label = classify(&train_labels, num_classes, k, knearest);
        printf("test index : %d, test label : %d, ",
               i, test_labels.data_ptr[i]);
        printf("training labels : ");
        for (j = 0; j < k; j++)
        {
            printf("%d ", train_labels.data_ptr[knearest[j]]);
        }
        printf(", predicted label : %d", predicted_label);
        printf("\n");
    }

    /* free up the training and test data sets */
    matrix_deinit(&train_images);
    matrix_deinit(&test_images);
    matrix_deinit(&train_labels);
    matrix_deinit(&test_labels);

    return 0;
}

