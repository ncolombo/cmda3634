#!/bin/bash
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=128
#SBATCH --cpus-per-task=1
#SBATCH -t 00:10:00
#SBATCH -p dev_q
#SBATCH -A cmda3634_rjh
#SBATCH -o mpi_results_final.out
#SBATCH --exclusive

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for openMP
module load gompi > /dev/null 2>&1

# Build the executable (if you haven't already)
mpicc -o mpi_classify_mnist mpi_classify_mnist.c matrix.c maxheap.c

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=true
export OMP_DYNAMIC=false

# run mpi_classify_mnist
mpirun -np $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./mpi_classify_mnist 21 10000

# The script will exit whether we give the "exit" command or not.
exit
