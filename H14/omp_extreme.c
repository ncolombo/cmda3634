#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "matrix.h"

int main(int argc, char **argv)
{
    int num_rows, num_cols;
    matrix_type test_set;

    /* get number to check and number of threads from the command line */
    if (argc != 3)
    {
        printf("Command usage: %s %s %s\n", argv[0], "num_check", "num_threads");
        return 1;
    }
    int num_check = atoi(argv[1]);
    int num_threads = atoi(argv[2]);

    if (num_check < 2)
        num_check = 2;
    if (num_check > 10000)
        num_check = 10000;

    /* read in the mnist test set of 10000 images */
    num_rows = 10000;
    num_cols = 784;
    matrix_init(&test_set, num_rows, num_cols);
    matrix_read_bin(&test_set, "t10k-images-idx3-ubyte", 16);

    /* start the timer */
    double start, end;
    start = omp_get_wtime();

    int max_dist_sq_bb[num_threads];
    int dist_sq_bb[num_threads];
    int max_row_1_bb[num_threads];
    int max_row_2_bb[num_threads];

#pragma omp parallel default(none) shared(num_check, test_set, max_dist_sq_bb, dist_sq_bb, max_row_1_bb, max_row_2_bb)
    {

        /* find the two extreme points */
        int i, j;
        int thread_dist_sq;
        int thread_max_dist_sq = 0;
        matrix_row_type row_i, row_j;
        int thread_max_row_1, thread_max_row_2;
        int thread = omp_get_thread_num();

#pragma omp for schedule(static)
        for (i = 0; i < num_check - 1; i++)
        {
            matrix_get_row(&test_set, &row_i, i);
            for (j = i + 1; j < num_check; j++)
            {
                matrix_get_row(&test_set, &row_j, j);
                thread_dist_sq = matrix_row_dist_sq(&row_i, &row_j);
                if (thread_dist_sq > thread_max_dist_sq)
                {
                    thread_max_dist_sq = thread_dist_sq;
                    thread_max_row_1 = i;
                    thread_max_row_2 = j;
                }
            }
        }
        max_dist_sq_bb[thread] = thread_max_dist_sq;
        dist_sq_bb[thread] = thread_dist_sq;
        max_row_1_bb[thread] = thread_max_row_1;
        max_row_2_bb[thread] = thread_max_row_2;
    }

    int winner = 0;
    for (int p = 1; p < num_threads; p++)
    {
        if (max_dist_sq_bb[p] > max_dist_sq_bb[winner])
        {
            winner = p;
        }
    }

    /* stop the timer */
    end = omp_get_wtime();

    /* print the results */
    printf("extreme digit indices are %d %d\n", max_row_1_bb[winner], max_row_2_bb[winner]);
    printf("max distance squared is %d\n", max_dist_sq_bb[winner]);
    printf("wall time used = %g sec\n", end - start);

    /* free the storage for the data set */
    matrix_deinit(&test_set);
}

