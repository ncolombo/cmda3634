#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

/* allocate and initialize to 0 a num_rows x num_cols matrix */
void matrix_init(matrix_type *mat_ptr, int num_rows, int num_cols)
{
    mat_ptr->num_rows = num_rows;
    mat_ptr->num_cols = num_cols;
    mat_ptr->data_ptr = (float *)calloc(num_rows * num_cols, sizeof(float));
#ifdef DEBUG
    if (mat_ptr->data_ptr == 0)
    {
        printf("error in matrix_init : calloc returned 0.\n");
        exit(1);
    }
#endif
}

/* deallocate a matrix and set all entries to 0 */
void matrix_deinit(matrix_type *mat_ptr)
{
    mat_ptr->num_rows = 0;
    mat_ptr->num_cols = 0;
    free(mat_ptr->data_ptr);
    mat_ptr->data_ptr = 0;
}

/* read a matrix from stdin */
void matrix_read(matrix_type *mat_ptr)
{
    int count = 0;
    float next;
    while (scanf("%g", &next) == 1)
    {
        if (count >= mat_ptr->num_rows * mat_ptr->num_cols)
        {
            printf("ERROR: stdin contains too many numbers\n");
            exit(1);
        }
        mat_ptr->data_ptr[count] = next;
        count += 1;
    }
    if (count < mat_ptr->num_rows * mat_ptr->num_cols)
    {
        printf("ERROR: stdin contains too rew numbers\n");
        exit(1);
    }
}

/* print a matrix to stdout */
void matrix_print(matrix_type *mat_ptr)
{
    int i;
    matrix_row_type row;
    printf("number of rows = %d\n", mat_ptr->num_rows);
    printf("number of columns = %d\n", mat_ptr->num_cols);
    for (i = 0; i < mat_ptr->num_rows; i++)
    {
        matrix_get_row(mat_ptr, &row, i);
        matrix_row_print(&row);
    }
}

/* get a row of a matrix (not a copy just a pointer into the data) */
void matrix_get_row(matrix_type *mat_ptr, matrix_row_type *row_ptr,
                    int row)
{
#ifdef DEBUG
    if ((row < 0) || (row >= mat_ptr->num_rows))
    {
        printf("error in matrix_get_row : row out of range\n");
        exit(1);
    }
#endif
    row_ptr->num_cols = mat_ptr->num_cols;
    row_ptr->data_ptr = &(mat_ptr->data_ptr[row * mat_ptr->num_cols]);
}

/* print a row of a matrix to stdout */
void matrix_row_print(matrix_row_type *row_ptr)
{
    int i;
    for (i = 0; i < row_ptr->num_cols; i++)
    {
        printf("%g ", row_ptr->data_ptr[i]);
    }
    printf("\n");
}

/* calculate the distance squared between two row vectors */
float matrix_row_dist_sq(matrix_row_type *row1_ptr,
                         matrix_row_type *row2_ptr)
{
#ifdef DEBUG
    if (row1_ptr->num_cols != row2_ptr->num_cols)
    {
        printf("error in matrix_row_dist_sq: number of columns mismatch\n");
    }
#endif

    float dist = 0;
    for (int i = 0; i < row1_ptr->num_cols; i++)
    {
        float v1 = row1_ptr->data_ptr[i];
        float v2 = row2_ptr->data_ptr[i];
        dist += (v1 - v2) * (v1 - v2);
    }

    return dist;
}
