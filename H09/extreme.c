#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

int main()
{
    int num_rows, num_cols;
    matrix_type matrix;
    matrix_row_type least;
    matrix_row_type greatest;
    matrix_row_type row1;
    matrix_row_type row2;

    /* read the number of columns and the number of rows */
    if (scanf("%d", &num_cols) != 1)
    {
        printf("ERROR: cannot read the number of columns\n");
        exit(1);
    }
    if (scanf("%d", &num_rows) != 1)
    {
        printf("ERROR: cannot read the number of rows\n");
        exit(1);
    }

    /* add code here */

    matrix_init(&matrix, num_rows, num_cols);
    matrix_read(&matrix);
    float highest = 0;
    for (int i = 0; i < num_rows; i++)
    {
        matrix_get_row(&matrix, &row1, i);
        if (i == 0)
        {
            least = row1;
            greatest = row1;
        }
        for (int j = 0; j < num_rows; j++)
        {
            matrix_get_row(&matrix, &row2, j);
            float dist = matrix_row_dist_sq(&row1, &row2);
            if (dist > highest)
            {
                highest = dist;
                least = row1;
                greatest = row2;
            }
        }
    }

    matrix_row_print(&least);
    matrix_row_print(&greatest);
}
