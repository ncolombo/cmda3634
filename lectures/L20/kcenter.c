#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include "matrix.h"

int main(int argc, char **argv) {

    int i, j, l;
    matrix_type A;
    matrix_row_type row;
    matrix_row_type* ktrial;
    matrix_row_type* kcenter;
    /* set initial best cost to the biggest float to avoid special case */  
    float center_cost = FLT_MAX;
    float dist_sq;
    float min_dist_sq;
    float max_min_dist_sq;

    /* seed the random number generator */
    srand(time(NULL));
  
    /* get filename, k, and m from command line */
    if (argc != 4) {
	printf ("Command usage : %s %s %s %s\n",argv[0],"filename","k","m");
	return 1;
    }

    char* filename = argv[1];
    int k = atoi(argv[2]);
    int m = atoi(argv[3]);
    ktrial = (matrix_row_type*)calloc(k,sizeof(matrix_row_type));
    kcenter = (matrix_row_type*)calloc(k,sizeof(matrix_row_type));  

    /* allocate, initialize to 0, and read the matrix */
    matrix_init_read (&A,filename);

    /* use randomization with m attempts to approx. the k-center problem */
    for (i=0;i<m;i++) {
	/* get a random array of k centers */
	for (j=0;j<k;j++) {
	    l = rand() % A.num_rows;
	    matrix_get_row(&A,&(ktrial[j]),l);
	}
	/* calculate the cost for this choice of k centers */
	max_min_dist_sq = 0;
	for (l=0;l<A.num_rows;l++) {
	    matrix_get_row(&A,&row,l);
	    min_dist_sq = FLT_MAX;
	    for (j=0;j<k;j++) {
		dist_sq = matrix_row_dist_sq(&(ktrial[j]),&row);
		if (dist_sq < min_dist_sq) {
		    min_dist_sq = dist_sq;
		}
	    }
	    if (min_dist_sq > max_min_dist_sq) {
		max_min_dist_sq = min_dist_sq;
	    }
	}
	/* check if we found a better set of k centers */
	if (max_min_dist_sq < center_cost) {
	    center_cost = max_min_dist_sq;
	    for (j=0;j<k;j++) {
		kcenter[j] = ktrial[j];
	    }
	}
    }

    /* print the approximate minimal cost for the k-center problem */
    printf ("# approximate minimal cost = %g\n",center_cost);
  
    /* print the approximate solution to the k center problem */
    for (j=0;j<k;j++) {
	matrix_row_print (&(kcenter[j]));
    }

    /* free the matrix */
    matrix_deinit (&A);

    /* free ktrial and kcenter */
    free(ktrial);
    free(kcenter);

}
