#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <mpi.h>

int main(int argc, char** argv) {

    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int i;
    int N = 5; /* length of message to exchange */
    int* in_msg; /* the received message */
    int* out_msg; /* the sent message */
    in_msg = (int*)calloc(N,sizeof(N));
    out_msg = (int*)calloc(N,sizeof(N));
    int tag = 999; /* message tag (unused for now) */
    int source, dest;
    MPI_Status status;

    /* generate N random integers between 0 and 999 (a random vector in R^N) */
    srand(time(NULL)+rank);
    for (i=0;i<N;i++) {
	out_msg[i] = rand() % 1000;
    }

    /* print out the local vector */
    printf ("vector for rank %d is ",rank);
    for (i=0;i<N;i++) {
	printf ("%d ",out_msg[i]);
    }
    printf ("\n");

    if (rank == 0) {
	for (i=1;i<size;i++) {
	    source = i;
	    MPI_Recv(in_msg,N,MPI_INT,source,tag,MPI_COMM_WORLD,&status);
	    for (int j=0;j<N;j++) {
		out_msg[j] += in_msg[j];
	    }
	}
	printf ("final sum is ");
	for (int j=0;j<N;j++) {
	    printf ("%d ",out_msg[j]);
	}
	printf ("\n");
    } else {
	dest = 0;
	MPI_Send(out_msg,N,MPI_INT,dest,tag,MPI_COMM_WORLD);
    }

    free(in_msg);
    free(out_msg);
    MPI_Finalize();
}
