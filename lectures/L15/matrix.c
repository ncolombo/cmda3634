#include <stdio.h>
#include <stdlib.h>
#include "matrix.h"

/* allocate and initialize to 0 a num_rows x num_cols matrix */
void matrix_init (matrix_type* mat_ptr, int num_rows, int num_cols) {
  mat_ptr->num_rows = num_rows;
  mat_ptr->num_cols = num_cols;
  mat_ptr->data_ptr = (float*) calloc (num_rows*num_cols, sizeof(float));
  #ifdef DEBUG
  if (mat_ptr->data_ptr == 0) {
    printf ("error in matrix_init : calloc returned 0.\n");
    exit(1);
  }
  #endif
}

/* free up a matrix */
void matrix_deinit (matrix_type* mat_ptr) {
  mat_ptr->num_rows = 0;
  mat_ptr->num_cols = 0;
  free(mat_ptr->data_ptr);
  mat_ptr->data_ptr = 0;
}

/* read a matrix from stdin */
void matrix_read (matrix_type* mat_ptr) {
  int count = 0;
  float next;
  while (scanf("%g",&next) == 1) {
    if (count >= mat_ptr->num_rows*mat_ptr->num_cols) {
      printf ("ERROR: stdin contains too many numbers\n");
      exit(1);
    }
    mat_ptr->data_ptr[count] = next;
    count += 1;
  }
  if (count < mat_ptr->num_rows*mat_ptr->num_cols) {
    printf ("ERROR: stdin contains too rew numbers\n");
    exit(1);    
  }
}

/* allocate and read a matrix from stdin */
void matrix_init_read (matrix_type* mat_ptr) {
    int num_rows, num_cols;

    /* read the number of columns and the number of rows */
    if (scanf("%d",&num_cols) != 1) {
	printf ("ERROR: cannot read the number of columns\n");    
	exit(1);
    }
    if (scanf("%d",&num_rows) != 1) {
	printf ("ERROR: cannot read the number of rows\n");
	exit(1);
    }
    
    /* allocate, initialize to 0, and read the matrix */
    matrix_init (mat_ptr,num_rows,num_cols);
    matrix_read(mat_ptr);
}

/* print a matrix to stdout */
void matrix_print (matrix_type* mat_ptr) {
  int i;
  matrix_row_type row;
  printf ("number of rows = %d\n",mat_ptr->num_rows);
  printf ("number of columns = %d\n",mat_ptr->num_cols);
  for (i=0;i<mat_ptr->num_rows;i++) {
    matrix_get_row (mat_ptr,&row,i);
    matrix_row_print (&row);
  }
}

/* get a row of the matrix (not a copy just a pointer into the data) */
void matrix_get_row (matrix_type* mat_ptr, matrix_row_type* row_ptr,
		     int row) {
  #ifdef DEBUG
  if ((row < 0) || (row >= mat_ptr->num_rows)) {
    printf ("error in matrix_get_row : row out of range\n");
    exit(1);
  }
  #endif
  row_ptr->num_cols = mat_ptr->num_cols;
  row_ptr->data_ptr = &(mat_ptr->data_ptr[row*mat_ptr->num_cols]);
}

/* print a row of the matrix */
void matrix_row_print (matrix_row_type* row_ptr) {
  int i;
  for (i=0;i<row_ptr->num_cols;i++) {
    printf ("%g ",row_ptr->data_ptr[i]);
  }
  printf ("\n");
}

/* calculate the distance squared between two row vectors */
float matrix_row_dist_sq (matrix_row_type* row1_ptr,
			  matrix_row_type* row2_ptr) {
  #ifdef DEBUG
  if (row1_ptr->num_cols != row2_ptr->num_cols) {
    printf ("error in matrix_row_dist_sq: number of columns mismatch\n");
  }
  #endif
  int i;
  float dist_sq = 0;
  float comp_diff = 0; /* component difference */
  for (i=0;i<row1_ptr->num_cols;i++) {
    comp_diff = row1_ptr->data_ptr[i]-row2_ptr->data_ptr[i];
    dist_sq += comp_diff*comp_diff;
  }
  return dist_sq;
}

