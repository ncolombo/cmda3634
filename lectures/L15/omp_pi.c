#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv) {

    /* get N and num_threads from command line */
    if (argc != 3) {
    	printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
    	return 1;
    }

    long int N = atol(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);
    long double sum_[num_threads];

    /* start the timer */
    double start, end;
    start = omp_get_wtime();

    long double sum = 0;
    for (long int i = 0; i < N; i++) {
	long double term = 4.0/(4*i+1) - 4.0/(4*i+3);
	sum += term;
    }
    
    /* stop the timer */
    end = omp_get_wtime();

    printf ("sum = %.10Lf\n",sum);
    printf ("pi  = 3.1415926535\n");
    printf ("wall time used = %g sec\n",end-start);
}
