#include <stdio.h>
#include <stdlib.h>
#include <float.h>
//#include <omp.h>
#include "matrix.h"

float compute_cost(matrix_type* mat_ptr, matrix_row_type* center1_ptr,
		   matrix_row_type* center2_ptr, matrix_row_type* center3_ptr) {
    matrix_row_type row_i;
    float dist_sq_1, dist_sq_2, dist_sq_3, min_dist_sq;
    float cost = 0;
    for (int i=0;i<mat_ptr->num_rows;i++) {
	matrix_get_row(mat_ptr,&row_i,i);
	dist_sq_1 = matrix_row_dist_sq(center1_ptr,&row_i);
	dist_sq_2 = matrix_row_dist_sq(center2_ptr,&row_i);
	dist_sq_3 = matrix_row_dist_sq(center3_ptr,&row_i);
	min_dist_sq = dist_sq_1;
	if (dist_sq_2 < min_dist_sq) {
	    min_dist_sq = dist_sq_2;
	}
	if (dist_sq_3 < min_dist_sq) {
	    min_dist_sq = dist_sq_3;
	}
	if (min_dist_sq > cost) {
	    cost = min_dist_sq;
	}
    }
    return cost;
}

int main(int argc, char **argv) {
    
    /* allocate and read the data set from stdin */
    matrix_type A;
    matrix_init_read (&A);

    /* get number of threads from command line */
    //    if (argc != 2) {
    //	printf ("Command usage: %s %s\n",argv[0],"num_threads");
    //	return 1;
    //    }
    //    int num_threads = atoi(argv[1]);
    //    omp_set_num_threads(num_threads);

    //    double start, end;
    //    start = omp_get_wtime();    

    /* solve the 3-center problem exactly */
    int i, j, k;
    float cost;
    float min_cost = FLT_MAX;
    matrix_row_type row_i, row_j, row_k;
    matrix_row_type center_1, center_2, center_3;
    for (i=0;i<A.num_rows-2;i++) {
	matrix_get_row(&A,&row_i,i);
	for (j=i+1;j<A.num_rows-1;j++) {
	    matrix_get_row(&A,&row_j,j);
	    for (k=j+1;k<A.num_rows;k++) {
		matrix_get_row(&A,&row_k,k);
		cost = compute_cost(&A,&row_i,&row_j,&row_k);
		if (cost < min_cost) {
		    min_cost = cost;
		    center_1 = row_i;
		    center_2 = row_j;
		    center_3 = row_k;
		}
	    }
	}
    }

    //    end = omp_get_wtime();

    /* free the matrix */
    matrix_deinit (&A);

    /* print the minimal cost for the 3-center problem (as a comment) */
    printf ("# minimal cost = %g\n",min_cost);
    
    /* print an optimal solution to the 3 center problem */
    matrix_row_print (&center_1);
    matrix_row_print (&center_2);
    matrix_row_print (&center_3);

    //    printf ("wall time used = %g sec\n",end-start);
    
}
