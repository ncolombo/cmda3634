#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <omp.h>
#include "matrix.h"

float compute_cost(matrix_type* mat_ptr, matrix_row_type* center1_ptr,
		   matrix_row_type* center2_ptr, matrix_row_type* center3_ptr) {
    matrix_row_type row_i;
    float dist_sq_1, dist_sq_2, dist_sq_3, min_dist_sq;
    float cost = 0;
    for (int i=0;i<mat_ptr->num_rows;i++) {
	matrix_get_row(mat_ptr,&row_i,i);
	dist_sq_1 = matrix_row_dist_sq(center1_ptr,&row_i);
	dist_sq_2 = matrix_row_dist_sq(center2_ptr,&row_i);
	dist_sq_3 = matrix_row_dist_sq(center3_ptr,&row_i);
	min_dist_sq = dist_sq_1;
	if (dist_sq_2 < min_dist_sq) {
	    min_dist_sq = dist_sq_2;
	}
	if (dist_sq_3 < min_dist_sq) {
	    min_dist_sq = dist_sq_3;
	}
	if (min_dist_sq > cost) {
	    cost = min_dist_sq;
	}
    }
    return cost;
}

int main(int argc, char **argv) {
    
    /* allocate and read the data set from stdin */
    matrix_type A;
    matrix_init_read (&A);

    /* get number of threads from command line */
    if (argc != 2) {
	printf ("Command usage: %s %s\n",argv[0],"num_threads");
    	return 1;
    }
    int num_threads = atoi(argv[1]);
    omp_set_num_threads(num_threads);

    double start, end;
    start = omp_get_wtime();    

    /* solve the 3-center problem exactly */

    float min_cost_[num_threads];
    matrix_row_type center_1_[num_threads];
    matrix_row_type center_2_[num_threads];
    matrix_row_type center_3_[num_threads];

#pragma omp parallel
    {
	int thread = omp_get_thread_num();
	int i, j, k;
	float cost;
	float min_cost = FLT_MAX;
	matrix_row_type row_i, row_j, row_k;
	matrix_row_type center_1, center_2, center_3;
	for (i=thread;i<A.num_rows-2;i+=num_threads) {
	    matrix_get_row(&A,&row_i,i);
	    for (j=i+1;j<A.num_rows-1;j++) {
		matrix_get_row(&A,&row_j,j);
		for (k=j+1;k<A.num_rows;k++) {
		    matrix_get_row(&A,&row_k,k);
		    cost = compute_cost(&A,&row_i,&row_j,&row_k);
		    if (cost < min_cost) {
		    min_cost = cost;
		    center_1 = row_i;
		    center_2 = row_j;
		    center_3 = row_k;
		    }
		}
	    }
	}
	/* communicate results */
	min_cost_[thread] = min_cost;
	center_1_[thread] = center_1;
	center_2_[thread] = center_2;
	center_3_[thread] = center_3;

    }

    /* pick the winning thread */
    int winning_thread=0;
    for (int i=1;i<num_threads;i++) {
	if (min_cost_[i] < min_cost_[winning_thread]) {
	    winning_thread = i;
	}
    }

    end = omp_get_wtime();

    /* free the matrix */
    matrix_deinit (&A);

    /* print the minimal cost for the 3-center problem (as a comment) */
    printf ("# minimal cost = %g\n",min_cost_[winning_thread]);
    
    /* print an optimal solution to the 3 center problem */
    matrix_row_print (&center_1_[winning_thread]);
    matrix_row_print (&center_2_[winning_thread]);
    matrix_row_print (&center_3_[winning_thread]);

    printf ("wall time used = %g sec\n",end-start);
    
}
