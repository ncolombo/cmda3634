#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/* compute the total stopping time for a given number n */
int total_stopping_time (int n) {
    long int a_i = n;
    int total = 0;
    while (a_i != 1) {
	if (a_i % 2 == 0) {
	    a_i = a_i/2;
	} else {
	    a_i = (3*a_i+1);
	}
	total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    /* get N from command line */
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"N");
	return 1;
    }

    int N = atoi(argv[1]);

    for (int n=1;n<=N;n++) {
	int total = total_stopping_time(n);
	printf ("%d %d\n",n,total);	
    }

    return 0;
}
