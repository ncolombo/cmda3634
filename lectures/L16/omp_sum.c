#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

int main(int argc, char **argv) {

    /* get N and num_threads from command line */
    if (argc != 3) {
	printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
	return 1;
    }

    long int N = atol(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    double start_time, end_time;
    start_time = omp_get_wtime();

    long int sum = 0;
    for (long int i = 1; i <= N;i++) {
	    sum += i;
    }

    end_time = omp_get_wtime();

    printf ("sum = %ld\n",sum);
    printf ("N*(N+1)/2 = %ld\n",N*(N+1)/2);
    printf ("wall time used = %g sec\n",end_time-start_time);
}
