#include <stdio.h>
#include <stdlib.h> 
#include "vector.h"
#include "maxheap.h"

int main (int argc, char** argv) {
    
    /* get number and k from the command line */
    if (argc != 3) {
	printf ("Command usage : %s %s %s\n",argv[0],"number","k");
	return 1;
    }
    float number = atof(argv[1]);
    int k = atoi(argv[2]);

    vector_type data;
    vector_init_read(&data);
    if (data.size < k) {
	printf ("Error: number of data points is less than k\n");
	exit(1);
    }

    /* free up the heap */
    maxheap_deinit(&heap);

    /* free up the data set */
    vector_deinit(&data);

    return 0;
}

