#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <time.h>
#include "matrix.h"

int extreme_approx(matrix_type* data_set_ptr, int start, int* extreme_pair) {

    /* for the given random start point find the two approximate extreme points */
    int i;
    int dist_sq;
    matrix_row_type row1, row2;
    int max_dist_sq;

    /* find point farthest from the start point */
    max_dist_sq = 0;
    matrix_get_row(data_set_ptr,&row1,start);
    for (i=0;i<data_set_ptr->num_rows-1;i++) {
	matrix_get_row(data_set_ptr,&row2,i);
	dist_sq = matrix_row_dist_sq(&row1,&row2);
	if (dist_sq > max_dist_sq) {
	    max_dist_sq = dist_sq;
	    extreme_pair[0] = i;
	}
    }
    
    /* find point farthest from the point farthest from the start point */
    max_dist_sq = 0;
    matrix_get_row(data_set_ptr,&row1,extreme_pair[0]);
    for (i=0;i<data_set_ptr->num_rows-1;i++) {
	matrix_get_row(data_set_ptr,&row2,i);
	dist_sq = matrix_row_dist_sq(&row1,&row2);
	if (dist_sq > max_dist_sq) {
	    max_dist_sq = dist_sq;
	    extreme_pair[1] = i;
	}
    }

    return max_dist_sq;
}


int main(int argc, char **argv) {

    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    matrix_type data_set;

    /* get number of random start points from command line */
    if (argc != 2) {
	printf ("Command usage: %s %s\n",argv[0],"num_rand");
	return 1;
    }
    int num_rand = atoi(argv[1]);


    /* read in the mnist training set */
    matrix_init (&data_set,60000,784);
    matrix_read_bin(&data_set,"train-images-idx3-ubyte",16);

    /* the random start points */
    int rand_start[num_rand];
    srand(time(NULL)+rank*size);
    for (int i=0;i<num_rand;i++) {
	rand_start[i] = rand() % data_set.num_rows;
    }
    
    /* the extreme pair */
    int extreme_pair[2];
    int max_dist_sq = 0;

    /* for each random start point find the two approximate extreme points */
    int extreme_pair_i[2];
    int max_dist_sq_i;
    for (int i=0;i<num_rand;i++) {
	max_dist_sq_i = extreme_approx(&data_set, rand_start[i], extreme_pair_i);
	/* see if we improved the overall max dist sq */
	if (max_dist_sq_i > max_dist_sq) {
	    max_dist_sq = max_dist_sq_i;
	    extreme_pair[0] = extreme_pair_i[0];
	    extreme_pair[1] = extreme_pair_i[1];
	}
	
    }

    /* print the most extreme of extreme pairs */
    printf ("from rank %d, the extreme pair is %d %d, max distance sq is %d\n",
	    rank,extreme_pair[0],extreme_pair[1],max_dist_sq);
    
    matrix_deinit(&data_set);
    MPI_Finalize();
}
