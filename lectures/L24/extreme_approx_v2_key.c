#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>
#include <time.h>
#include "matrix.h"

int extreme_approx(matrix_type* data_set_ptr, int start, int* extreme_pair) {

    int max_dist_sq[2];
    max_dist_sq[0] = 0;
    max_dist_sq[1] = 0;

#pragma omp parallel default(none) shared(data_set_ptr,start,extreme_pair,max_dist_sq)
	{

	    /* for the given random start point find the two approximate extreme points */
	    int i;
	    int dist_sq;
	    int thread_max_dist_sq = 0;
	    int thread_extreme_pair[2];
	    matrix_row_type row1, row2;

	    /* find point farthest from the start point */
	    matrix_get_row(data_set_ptr,&row1,start);
#pragma omp for schedule(static) nowait
	    for (i=0;i<data_set_ptr->num_rows-1;i++) {
		matrix_get_row(data_set_ptr,&row2,i);
		dist_sq = matrix_row_dist_sq(&row1,&row2);
		if (dist_sq > thread_max_dist_sq) {
		    thread_max_dist_sq = dist_sq;
		    thread_extreme_pair[0] = i;
		}
	    }
#pragma omp critical 
	    {
		if (thread_max_dist_sq > max_dist_sq[0]) {
		    max_dist_sq[0] = thread_max_dist_sq;
		    extreme_pair[0] = thread_extreme_pair[0];
		}
	    }	    
#pragma omp barrier
	    /* find point farthest from the point farthest from the start point */
	    thread_max_dist_sq = 0;
	    matrix_get_row(data_set_ptr,&row1,extreme_pair[0]);
#pragma omp for schedule(static) nowait
	    for (i=0;i<data_set_ptr->num_rows-1;i++) {
		matrix_get_row(data_set_ptr,&row2,i);
		dist_sq = matrix_row_dist_sq(&row1,&row2);
		if (dist_sq > thread_max_dist_sq) {
		    thread_max_dist_sq = dist_sq;
		    thread_extreme_pair[1] = i;
		}
	    }
#pragma omp critical 
	    {
		if (thread_max_dist_sq > max_dist_sq[1]) {
		    max_dist_sq[1] = thread_max_dist_sq;
		    extreme_pair[1] = thread_extreme_pair[1];
		}
	    }	    
	}

    return max_dist_sq[1];
}


int main(int argc, char **argv) {

    int rank, size;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    matrix_type data_set;

    /* get number of random start points from command line */
    if (argc != 2) {
	printf ("Command usage: %s %s\n",argv[0],"num_rand");
	return 1;
    }
    int num_rand = atoi(argv[1]);

    /* read in the mnist 1 million training set */
    matrix_init (&data_set,1000000,784);
    matrix_read_bin(&data_set,"mnist1m-images-idx3-ubyte",16);

    /* the random start points */
    int rand_start[num_rand];
    int rand_starts[num_rand*size];
    if (rank==0) {
	//	srand(time(NULL));
	srand(1234);
	for (int i=0;i<num_rand*size;i++) {
	    rand_starts[i] = rand() % data_set.num_rows;
	}
    }
    MPI_Scatter(rand_starts,num_rand,MPI_INT,rand_start,num_rand,MPI_INT,0,MPI_COMM_WORLD);

    /* the extreme pair */
    int extreme_pair[2];
    int max_dist_sq = 0;

    /* start the timer */
    double start, end;
    start = omp_get_wtime();

    /* for each random start point find the two approximate extreme points */
    int extreme_pair_i[2];
    int max_dist_sq_i;
    for (int i=0;i<num_rand;i++) {
	max_dist_sq_i = extreme_approx(&data_set, rand_start[i], extreme_pair_i);
	/* see if we improved the overall max dist sq */
	if (max_dist_sq_i > max_dist_sq) {
	    max_dist_sq = max_dist_sq_i;
	    extreme_pair[0] = extreme_pair_i[0];
	    extreme_pair[1] = extreme_pair_i[1];
	}
    }
    printf ("rank = %d, max_dist_sq = %d\n",rank,max_dist_sq);

    int extreme_pairs[2*size];
    MPI_Gather(extreme_pair,2,MPI_INT,extreme_pairs,2,MPI_INT,0,MPI_COMM_WORLD);    

    if (rank == 0) {
	int winning_rank = 0;
	for (int i=1;i<size;i++) {
	    matrix_row_type row1, row2;
	    matrix_get_row(&data_set,&row1,extreme_pairs[2*i]);
	    matrix_get_row(&data_set,&row2,extreme_pairs[2*i+1]);
	    int dist_sq = matrix_row_dist_sq(&row1,&row2);
	    if (dist_sq > max_dist_sq) {
		max_dist_sq = dist_sq;
		extreme_pair[0] = extreme_pairs[2*i];
		extreme_pair[1] = extreme_pairs[2*i+1];
		winning_rank = i;
	    }
	}
	/* print the most extreme of extreme pairs */
	printf ("from winning rank %d, the extreme pair is %d %d, max distance sq is %d\n",
		winning_rank,extreme_pair[0],extreme_pair[1],max_dist_sq);
    }

    /* stop timer */
    end = omp_get_wtime();
    if (rank==0) {
	printf ("wall time used = %g sec\n",end-start);
    }
    
    matrix_deinit(&data_set);
    MPI_Finalize();
}
