#!/bin/bash
#SBATCH --nodes=2
#SBATCH --ntasks-per-node=8
#SBATCH --cpus-per-task=1
#SBATCH -t 00:10:00
#SBATCH -p dev_q
#SBATCH -A cmda3634_rjh
#SBATCH -o results_v2.out
#SBATCH --exclusive

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load gompi > /dev/null 2>&1

# Build the executable
mpicc -o extreme_approx_v2 extreme_approx_v2.c matrix.c -fopenmp

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK
export OMP_PROC_BIND=true
export OMP_DYNAMIC=false

# run knearest_mnist
mpirun -np $SLURM_NTASKS --map-by ppr:$SLURM_NTASKS_PER_NODE:node ./extreme_approx_v2 8

# The script will exit whether we give the "exit" command or not.
exit
