#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/* compute the total stopping time for a given number n */
int total_stopping_time (int n) {
    long int a_i = n;
    int total = 0;
    while (a_i != 1) {
	if (a_i % 2 == 0) {
	    a_i = a_i/2;
	} else {
	    a_i = (3*a_i+1);
	}
	total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    /* get N and num_threads from command line */
    if (argc != 3) {
	printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
	return 1;
    }

    int N = atoi(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    double start_time, end_time;
    start_time = omp_get_wtime();

    int max_start = 1;
    int max_total = 0;

#pragma omp parallel default(none) shared(N,max_start,max_total,num_threads)
    {
	int thread = omp_get_thread_num();
	int thread_max_start = 1;
	int thread_max_total = 0;
	int num_tested = 0;
	for (int n=1+thread;n<=N;n+=num_threads) {
	    int total = total_stopping_time(n);
	    num_tested += 1;
	    if (total > thread_max_total) {
		thread_max_start = n;
		thread_max_total = total;
	    }
	}
#pragma omp barrier /* explicit barrier */
	printf ("Thread %d tested %d numbers\n",thread,num_tested);
#pragma omp critical(update_global_max)
	{
	    if (thread_max_total > max_total) {
		max_start = thread_max_start;
		max_total = thread_max_total;
	    }
	}
    } /* implicit barrier */
    printf ("Parallel region complete\n");

    end_time = omp_get_wtime();

    printf ("The starting value less than or equal to %d\n",N);
    printf ("  having the largest total stopping time is %d\n",max_start);
    printf ("  which has %d steps\n",max_total);
    printf ("wall time used = %g sec\n",end_time-start_time);

    return 0;
}



