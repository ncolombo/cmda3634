#include <stdio.h>
#include <stdlib.h>
#include <omp.h>

/* compute the total stopping time for a given number n */
int total_stopping_time (int n) {
    long int a_i = n;
    int total = 0;
    while (a_i != 1) {
	if (a_i % 2 == 0) {
	    a_i = a_i/2;
	} else {
	    a_i = (3*a_i+1);
	}
	total += 1;
    }
    return total;
}

int main(int argc, char **argv) {

    /* get N and num_threads from command line */
    if (argc != 3) {
	printf ("Command usage : %s %s %s\n",argv[0],"N","num_threads");
	return 1;
    }

    int N = atoi(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    double start_time, end_time;
    start_time = omp_get_wtime();

    int max_starts_bb[num_threads];
    int max_totals_bb[num_threads];

#pragma omp parallel default(none) shared(N,max_starts_bb,max_totals_bb)
    {
	int thread_max_start = 1;
	int thread_max_total = 0;
	int thread = omp_get_thread_num();
#pragma omp for schedule(dynamic,1000)
	for (int n=1;n<=N;n++) {
	    int total = total_stopping_time(n);
	    if (total > thread_max_total) {
		thread_max_start = n;
		thread_max_total = total;
	    }
	}
	max_starts_bb[thread] = thread_max_start;
	max_totals_bb[thread] = thread_max_total;
    } 

    int winner = 0;
    for (int p=1;p<num_threads;p++) {
	if (max_totals_bb[p] > max_totals_bb[winner]) {
	    winner = p;
	}
    }

    end_time = omp_get_wtime();

    printf ("The starting value less than or equal to %d\n",N);
    printf ("  having the largest total stopping time is %d\n",
	    max_starts_bb[winner]);
    printf ("  which has %d steps\n",
	    max_totals_bb[winner]);
    printf ("wall time used = %g sec\n",end_time-start_time);

    return 0;
}



