#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include "matrix.h"

int main(int argc, char **argv) {
    int num_rows, num_cols;
    matrix_type train_set;

    /* get num to check from the command line */
    if (argc != 3) {
	printf ("Command usage: %s %s %s\n",argv[0],"num_check","num_threads");
	return 1;
    }
    int num_check = atoi(argv[1]);
    int num_threads = atoi(argv[2]);
    omp_set_num_threads(num_threads);

    /* read in the mnist test set */
    num_rows = 60000;
    num_cols = 784;
    matrix_init (&train_set,num_rows,num_cols);
    matrix_read_bin(&train_set,"train-images-idx3-ubyte",16);

    /* start the timer */
    double start, end;
    start = omp_get_wtime();

    /* find the two approximate extreme points */
    int i,j;
    int dist_sq;
    int max_dist_sq;
    matrix_row_type row, row_i;
    int max_row_1, max_row_2;

    /* find the point farthest from the first point */
    max_dist_sq = 0;
    matrix_get_row(&train_set,&row,0);
    for (i=0;i<num_check-1;i++) {
	matrix_get_row(&train_set,&row_i,i);
	dist_sq = matrix_row_dist_sq(&row,&row_i);
	if (dist_sq > max_dist_sq) {
	    max_dist_sq = dist_sq;
	    max_row_1 = i;
	}
    }

    /* find the point farthest from the point farthest from the first point */
    max_dist_sq = 0;
    matrix_get_row(&train_set,&row,max_row_1);
    for (i=0;i<num_check-1;i++) {
	matrix_get_row(&train_set,&row_i,i);
	dist_sq = matrix_row_dist_sq(&row,&row_i);
	if (dist_sq > max_dist_sq) {
	    max_dist_sq = dist_sq;
	    max_row_2 = i;
	}
    }

    end = omp_get_wtime();

    /* print the extreme row vector indices */
    printf ("extreme digit indices are %d %d\n",max_row_1,max_row_2);
    printf ("max distance squared is %d\n",max_dist_sq);
    printf ("wall time used = %g sec\n",end-start);

    matrix_deinit(&train_set);
}
