import sys
import numpy as np
import matplotlib.pyplot as plt

def distance_sq(v,w):
    return np.inner(v-w,v-w)

def centers_cost(data,center1,center2):
    cost = 0
    num_cities = len(data)
    for i in range(num_cities):
        distance1 = distance_sq(data[i],data[center1])
        distance2 = distance_sq(data[i],data[center2])
        min_distance = distance1
        if (distance2 < distance1):
            min_distance = distance2
        if (min_distance > cost):
            cost = min_distance
    return cost

# read the matrix file
data = np.loadtxt(sys.stdin,skiprows=2)

# solve the 2-center problem 
min_cost = float("inf")
num_cities = len(data)
for i in range(num_cities-1):
    center1 = i
    for j in range(i+1,num_cities):
        center2 = j
        cost = centers_cost(data,center1,center2)
        if (cost<min_cost):
            min_cost = cost
            best1 = center1
            best2 = center2

# print the results
print ("# minimum cost = ",min_cost)
print (data[best1])
print (data[best2])

# plot the data
plt.gca().set_aspect('equal')
plt.scatter(data[:,0],data[:,1],s=10,color='black')
plt.scatter(data[best1,0],data[best1,1],s=100,color='maroon')
plt.scatter(data[best2,0],data[best2,1],s=100,color='orange')

# save the plot as an image
plt.savefig("2center.png")

#v = np.array([1,2,3])
#w = np.array([2,3,4])
#A = np.array([[1,2,3],[4,5,6],[7,8,9]])
#print (distance_sq(v,w))




