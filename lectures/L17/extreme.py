# Read MNIST test data set and store it as a 10000 by 784 matrix.
# There are 10000 images, each of which is 28 x 28 = 784 pixels
# Plot 2 of the 10000 images as a .png file for viewing.

import sys
import numpy as np
import matplotlib.pyplot as plt

if (len(sys.argv) != 3):
    print ("Not enough command line arguments")
    quit()

idx1 = int(sys.argv[1])
idx2 = int(sys.argv[2])

f = open('t10k-images-idx3-ubyte','rb')

image_size = 28
num_images = 10000

f.read(16)
buf = f.read(image_size * image_size * num_images)
data = np.frombuffer(buf, dtype=np.uint8).astype(np.float32)
data = data.reshape(num_images, image_size * image_size)

plt.rcParams['figure.figsize'] = (20, 20)
plt.rc('xtick', labelsize=20) 
plt.rc('ytick', labelsize=20) 
plt.rcParams['axes.facecolor']='white'
plt.rcParams['savefig.facecolor']='white'
image1 = np.asarray(data[idx1]).reshape(image_size,image_size)
image2 = np.asarray(data[idx2]).reshape(image_size,image_size)
f, axarr = plt.subplots(1,2)
axarr[0].imshow(image1,cmap='gray',vmin=0, vmax=255, interpolation='none')
axarr[1].imshow(image2,cmap='gray',vmin=0, vmax=255, interpolation='none')
plt.savefig("mnist2.png",bbox_inches='tight')
