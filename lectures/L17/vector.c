#include <stdio.h>
#include <stdlib.h>
#include "vector.h"

/* allocate and initialize to 0 a vector with size components */
void vector_init (vector_type* vector_ptr, int size) {
    vector_ptr->size = size;
    vector_ptr->data_ptr = (float*) calloc (size, sizeof(float));
#ifdef DEBUG
    if (vector_ptr->data_ptr == 0) {
	printf ("error in vector_init : calloc returned 0.\n");
	exit(1);
    }
#endif
}

/* free up a vector */
void vector_deinit (vector_type* vector_ptr) {
    vector_ptr->size = 0;
    free(vector_ptr->data_ptr);
    vector_ptr->data_ptr = 0;
}

/* read a vector from stdin */
void vector_read (vector_type* vector_ptr) {
    int count = 0;
    float next;
    while (scanf("%g",&next) == 1) {
	if (count >= vector_ptr->size) {
	    printf ("ERROR: stdin contains more than size numbers\n");
	    exit(1);
	}
    vector_ptr->data_ptr[count] = next;
    count += 1;
    }
    if (count < vector_ptr->size) {
	printf ("ERROR: stdin contains too rew numbers\n");
	exit(1);    
  }
}

/* allocate and read a vector from stdin */
void vector_init_read (vector_type* vector_ptr) {
    int size;
    
    /* read the size */
    if (scanf("%d",&size) != 1) {
	printf ("ERROR: cannot read the size\n");    
	exit(1);
    }
    
    /* allocate, initialize to 0, and read the vector */
    vector_init (vector_ptr,size);
    vector_read (vector_ptr);
}

/* print a vector */
void vector_print (vector_type* vec_ptr) {
    int i;
    printf ("size = %d\n",vec_ptr->size);
    for (i=0;i<vec_ptr->size;i++) {
	printf ("%g ",vec_ptr->data_ptr[i]);
    }
    printf ("\n");
}


