/* vector.h */

/* make sure this header file has not been included already */
#ifndef __VECTOR
#define __VECTOR 1

typedef struct vector_s {
    int size;
    float* data_ptr;
} vector_type;

/* allocate and initialize to 0 a vector with size components */
void vector_init (vector_type* vec_ptr, int size);

/* free up a vector */
void vector_deinit (vector_type* vec_ptr);

/* read a vector from stdin */
void vector_read (vector_type* vec_ptr);

/* allocate and read a matrix from stdin */
void vector_init_read (vector_type* vec_ptr);

/* print a vector */
void vector_print (vector_type* vec_ptr);

#endif
