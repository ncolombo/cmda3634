#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <omp.h> /* to use the time functions */

int main(int argc, char **argv) {
    double start, end;
    long double term;
    long double sum = 0;

    /* get N from command line */
    if (argc != 2) {
	printf ("Command usage : %s %s\n",argv[0],"N");
	return 1;
    }
    long int N = atol(argv[1]);

    /* read start time */
    start = omp_get_wtime();

    for (long int i = 0; i < N; i++) {
	term = 4.0/(4*i+1) - 4.0/(4*i+3);
	sum += term;
    }

    /* read end time */
    end = omp_get_wtime();

    printf ("sum = %.10Lf\n",sum);
    printf ("pi  = 3.1415926535\n");
    printf ("wall time used = %g sec\n",end-start);
}
