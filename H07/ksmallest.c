#include <stdio.h>
#include <stdlib.h> /* necessary to use exit(1) */

#define MAX_SIZE 1000000
#define MAX_K 1000000

/* The maxheap structure */
typedef struct maxheap_s
{
    float array[MAX_SIZE];
    int size;
} maxheap_type;

/* insert a number into our heap */
void maxheap_insert(maxheap_type *heap_ptr, float new)
{
    int done_heapify = 0;
    int current_index, parent_index;
    float temp;

    /* check to see if there is room for the new number */
    if (heap_ptr->size == MAX_SIZE)
    {
        printf("Error: cannot process more than MAX_SIZE=%d numbers!\n",
               MAX_SIZE);
        exit(1); /* terminate program abnormally */
    }

    /* insert at the end (remember that the array is 0 based) */
    heap_ptr->size += 1;
    heap_ptr->array[heap_ptr->size - 1] = new;

    /* set the current index */
    current_index = heap_ptr->size - 1;

    /* if heap contains 1 node we are done since it is already a maxheap */
    if (current_index == 0)
    {
        done_heapify = 1;
    }

    /* climb the tree swapping as necessary to maintain maxheap property */
    while (done_heapify == 0)
    {
        parent_index = (current_index - 1) / 2;
        /* check to see if we need to swap value with parent */
        if (heap_ptr->array[parent_index] < heap_ptr->array[current_index])
        {
            temp = heap_ptr->array[current_index];
            heap_ptr->array[current_index] = heap_ptr->array[parent_index];
            heap_ptr->array[parent_index] = temp;
        }
        else
        {
            /* if parent value is more than or equal to our value we are done */
            done_heapify = 1;
        }
        /* climb the tree */
        current_index = parent_index;
        /* if we are at root we are done */
        if (current_index == 0)
        {
            done_heapify = 1;
        }
    }
}

/* find the index of the smallest child value */
/* we assume that there is at least one child */
int index_of_largest_child(maxheap_type *heap_ptr, int current_index)
{
    int left_index, right_index;
    left_index = current_index * 2 + 1;
    right_index = current_index * 2 + 2;
    /* handle the single child case */
    if (right_index > heap_ptr->size - 1)
    {
        return left_index;
    }
    /* handle the two child case */
    if (heap_ptr->array[right_index] > heap_ptr->array[left_index])
    {
        return right_index;
    }
    else
    {
        return left_index;
    }
}

/* remove the largest value (root value) from the heap */
void maxheap_remove_root(maxheap_type *heap_ptr)
{
    int done_heapify = 0;
    int current_index, child_index;
    float temp;

    /* nothing to do for an empty heap */
    if (heap_ptr->size == 0)
    {
        return;
    }

    /* handle the case where the heap has one element */
    if (heap_ptr->size == 1)
    {
        heap_ptr->size = 0;
        return;
    }

    /* move the last element to the root and decrease size by 1 */
    heap_ptr->array[0] = heap_ptr->array[heap_ptr->size - 1];
    heap_ptr->size -= 1;

    /* start at the root */
    current_index = 0;

    /* if heap contains 1 node we are done since it is already a maxheap */
    if (heap_ptr->size == 1)
    {
        return;
    }

    /* descend the tree swapping as necessary to maintain maxheap prop. */
    while (done_heapify == 0)
    {
        child_index = index_of_largest_child(heap_ptr, current_index);
        if (heap_ptr->array[current_index] < heap_ptr->array[child_index])
        {
            temp = heap_ptr->array[current_index];
            heap_ptr->array[current_index] = heap_ptr->array[child_index];
            heap_ptr->array[child_index] = temp;
        }
        else
        {
            /* done if largest child value is greater than or equal to ours */
            done_heapify = 1;
        }
        /* descend the tree */
        current_index = child_index;
        /* if there are no children, we are done */
        if (current_index * 2 + 1 > heap_ptr->size - 1)
        {
            done_heapify = 1;
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 2)
    {
        printf("Command usage : %s %s\n", argv[0], "k");
        return 1;
    }
    int k = atoi(argv[1]);

    /* check to see if k is less than a million */
    if (k > MAX_K)
    {
        printf("Error: cannot find more than MAX_K=%d smallest numbers!\n",
               MAX_K);
        exit(1); /* terminate program abnormally */
    }

    maxheap_type heap;
    heap.size = 0;
    float next;

    /* read first k numbers into a heap */
    int count = 0;
    while (scanf("%g", &next) == 1)
    {
        if (count < k)
        {
            maxheap_insert(&heap, next);
            count += 1;
        }
        else
        {
            if (heap.array[0] > next)
            {
                maxheap_remove_root(&heap);
                maxheap_insert(&heap, next);
            }
            count += 1;
        }
    }

    /* add the numbers in decending order to an array */
    int order_size = count;
    if (k < count)
    {
        order_size = k;
    }
    float order[order_size];
    int count2 = order_size - 1;
    while (heap.size > 0)
    {
        order[count2] = heap.array[0];
        count2 -= 1;
        maxheap_remove_root(&heap);
    }
    /* print out the numbers in sorted order */
    for (int i = 0; i < order_size; i++)
    {
        printf("%g\n", order[i]);
    }

    return 0;
}
