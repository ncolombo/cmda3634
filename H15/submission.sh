#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=1
#SBATCH -t 00:10:00
#SBATCH -p dev_q
#SBATCH -A cmda3634_rjh
#SBATCH -o results.out

# Go to the directory where the job was submitted
cd $SLURM_SUBMIT_DIR

# Load the modules we need for MPI
module load gompi

# Build the executable (if you haven't already)
mpicc -o mpi_kcenter mpi_kcenter.c matrix.c

# OpenMP settings
export OMP_NUM_THREADS=$SLURM_NTASKS
export OMP_PROC_BIND=true
export OMP_DYNAMIC=false

# run kcenter on nodes*ntasks-per-node cpus
mpirun -np $SLURM_NTASKS ./mpi_kcenter 2d100.dat 9 2000000 1234

# The script will exit whether we give the "exit" command or not.
exit
