#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <time.h>
#include <mpi.h>
#include "matrix.h"

float calculate_cost(matrix_type *data_set_ptr, int k, int *ktrial)
{

    int i, j;
    float dist_sq;
    float min_dist_sq;
    float max_min_dist_sq;
    matrix_row_type row_i, row_j;

    /* calculate the cost for this choice of k centers */
    max_min_dist_sq = 0;
    for (i = 0; i < data_set_ptr->num_rows; i++)
    {
        matrix_get_row(data_set_ptr, &row_i, i);
        min_dist_sq = FLT_MAX;
        for (j = 0; j < k; j++)
        {
            matrix_get_row(data_set_ptr, &row_j, ktrial[j]);
            dist_sq = matrix_row_dist_sq(&row_i, &row_j);
            if (dist_sq < min_dist_sq)
            {
                min_dist_sq = dist_sq;
            }
        }
        if (min_dist_sq > max_min_dist_sq)
        {
            max_min_dist_sq = min_dist_sq;
        }
    }
    return max_min_dist_sq;
}

int main(int argc, char **argv)
{

    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    int i, j;
    matrix_type data_set;
    matrix_row_type row;
    int *kbest;
    int *ktrial;
    /* set initial best cost to the biggest float to avoid special case */
    float best_cost = FLT_MAX;
    float trial_cost;

    /* get filename, k, m, and seed (optional) from command line */
    if ((argc != 4) && (argc != 5))
    {
        printf("Command usage : %s %s %s %s %s\n", argv[0], "filename",
               "k", "m", "(seed)");
        return 1;
    }
    char *filename = argv[1];
    int k = atoi(argv[2]);
    int m = atoi(argv[3]);
    int seed = time(NULL);
    if (argc == 5)
    {
        seed = atoi(argv[4]);
    }

    /* seed the random number generator so each rank gets a different seed */
    srand(seed + size * rank);

    /* allocate arrays to store the k best centers and the k trial centers */
    kbest = (int *)calloc(k, sizeof(int));
    ktrial = (int *)calloc(k, sizeof(int));

    /* allocate, initialize to 0, and read the matrix */
    matrix_init_read(&data_set, filename);

    if (rank == 55)
    {
        int index = 0;
        /* use randomization with m attempts to approx. the k-center problem */
        for (i = 0; i < m; i++)
        {
            /* get a random array of k centers */
            for (j = 0; j < k; j++)
            {
                ktrial[j] = rand() % data_set.num_rows;
            }
            /* calculate the cost of the trial */
            trial_cost = calculate_cost(&data_set, k, ktrial);

            /* check if we found a better set of k centers */
            if (trial_cost < best_cost)
            {
                best_cost = trial_cost;
                index = i;
                for (j = 0; j < k; j++)
                {
                    kbest[j] = ktrial[j];
                }
            }
        }

        /* print the approximate minimal cost for the k-center problem */
        printf("# approximate minimal cost = %g", best_cost);
        printf(" (found by rank %d)\n", rank);

        /* print the approximate solution to the k center problem */
        printf("# indices: ");
        for (j = 0; j < k; j++)
        {
            printf("%d ", kbest[j]);
        }
        printf("\n");
        for (j = 0; j < k; j++)
        {
            matrix_get_row(&data_set, &row, kbest[j]);
            matrix_row_print(&row);
        }
    }

    /* free the matrix */
    matrix_deinit(&data_set);

    /* free kbest and ktrial */
    free(kbest);
    free(ktrial);

    MPI_Finalize();
}

