import sys
import numpy as np
import matplotlib.pyplot as plt

def read_rows (f,data):
    counter = 0
    row = 0;
    col = 0;
    for line in f:
        if (not line.startswith("#")):
            for s in line.split():
                num = float(s)
                data[row,col] = num
                col += 1
                if (col == num_cols):
                    row += 1
                    col = 0
    return row

with sys.stdin as f:
    max_centers = 10
    firstline = f.readline()
    num_cols = int(firstline)
    secondline = f.readline()
    num_rows = int(secondline)
    data = np.zeros((num_rows+max_centers,num_cols))
    rows_read = read_rows(f,data)
    points = data[:num_rows]
    centers = data[num_rows:rows_read]
    center_nums = range(len(centers))
    point_centers = np.zeros(num_rows)
    distance_sq = np.zeros(len(centers))
    for i in range(num_rows):
        for j in range(len(centers)):
            distance_sq[j] = np.inner(points[i]-centers[j],
                                  points[i]-centers[j])
        point_centers[i] = np.argmin(distance_sq)
    plt.gca().set_aspect('equal')
    plt.scatter (points[:,0],points[:,1],
                 c=point_centers,cmap="tab10",s=10)
    plt.scatter (centers[:,0],centers[:,1],
                 c=center_nums,cmap="tab10",s=100)
    plt.savefig("view2d.png")


                
