#include <stdio.h>

int main()
{
    float input;
    float smallest;
    float smallest2;

    if (scanf("%f", &input) == 1)
    {
        smallest = input;
        if (scanf("%f", &input) == 1)
        {
            if (smallest < input)
            {
                smallest2 = input;
            }
            else
            {
                smallest2 = smallest;
                smallest = input;
            }
            while (scanf("%f", &input) == 1)
            {
                if (smallest > input)
                {
                    smallest2 = smallest;
                    smallest = input;
                }
                else if (smallest2 > input)
                {
                    smallest2 = input;
                }
            }
            printf("The smallest number is %f\n", smallest);
            printf("The second smallest number is %f\n", smallest2);
        }
        else
        {
            printf("There is only one number to process.\n");
            printf("The smallest number is %f\n", smallest);
        }
    }
    else
    {
        printf("There are no numbers to order.\n");
    }
}
