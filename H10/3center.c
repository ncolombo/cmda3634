#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include "matrix.h"


int main()
{
  int num_rows, num_cols;
  /* set initial best cost to the biggest float to avoid special case */
  float center_cost = FLT_MAX;
  matrix_type matrix;

  /* read the number of columns and the number of rows */
  if (scanf("%d", &num_cols) != 1)
  {
    printf("ERROR: cannot read the number of columns\n");
    exit(1);
  }
  if (scanf("%d", &num_rows) != 1)
  {
    printf("ERROR: cannot read the number of rows\n");
    exit(1);
  }

  matrix_init(&matrix, num_rows, num_cols);
  matrix_read(&matrix);

  matrix_row_type best1;
  matrix_row_type best2;
  matrix_row_type best3;
  for (int i = 0; i < (num_rows - 2); i++)
  {
    matrix_row_type center1;
    matrix_get_row(&matrix, &center1, i);
    for (int j = (i + 1); j < (num_rows - 1); j++)
    {
      matrix_row_type center2;
      matrix_get_row(&matrix, &center2, j);
      for (int k = (i + 2); k < num_rows; k++)
      {
        matrix_row_type center3;
        matrix_get_row(&matrix, &center3, k);
        float cost = 0;
        for (int l = 0; l < num_rows; l++)
        {
          matrix_row_type current;
          matrix_get_row(&matrix, &current, l);
          float dis1 = matrix_row_dist_sq(&current, &center1);
          float dis2 = matrix_row_dist_sq(&current, &center2);
          float dis3 = matrix_row_dist_sq(&current, &center3);
          float min_d = dis1;
          if (dis2 < dis1 && dis2 < dis3)
          {
            min_d = dis2;
          }
          else if (dis3 < dis1 && dis3 < dis2)
          {
            min_d = dis3;
          }
          if (min_d > cost)
          {
            cost = min_d;
          }
        }
        if (cost < center_cost)
        {
          center_cost = cost;
          best1 = center1;
          best2 = center2;
          best3 = center3;
        }
      }
    }
  }

  printf("# minimal cost = %g\n", center_cost);
  matrix_row_print(&best1);
  matrix_row_print(&best2);
  matrix_row_print(&best3);
}
